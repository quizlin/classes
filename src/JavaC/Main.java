package JavaC;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


import java.io.IOException;





public class Main {

    public static String PokemonToJSON(Pokemon Pokemon) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(Pokemon);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Pokemon JSONToPokemon(Pokemon s) {

        ObjectMapper mapper = new ObjectMapper();
        Pokemon Pokemon = null;

        try {
            Pokemon = mapper.readValue("s", Pokemon.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Pokemon;
    }

    public static void main(String[] args) {

        Pokemon Poke = new Pokemon();
        Poke.setName("Charmander");
        Poke.setNumber(56);

        Pokemon Pokee = Main.JSONToPokemon(Poke);
        System.out.println(Pokee);

        String Poke2 = Main.PokemonToJSON(Pokee);
        System.out.println(Poke2);
    }

}
