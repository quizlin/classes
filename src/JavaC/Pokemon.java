package JavaC;

public class Pokemon {

    private String name;
    private int number;



    public void setName(String name) {
        this.name = name;
    }



    public void setNumber(int number) {
        this.number = number;
    }

    public String toString() {
        return "Name: " + name + " Number: " + number;
    }
}
